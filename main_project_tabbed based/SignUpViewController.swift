//
//  SignUpViewController.swift
//  main_project_tabbed based
//
//  Created by Nusum,Koti Reddy on 10/6/16.
//  Copyright © 2016 Nusum,Koti Reddy. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var LastNameTF: UITextField!
    @IBOutlet weak var PhoneTF: UITextField!
    @IBOutlet weak var EmailAddressTF: UITextField!
    
    @IBOutlet weak var BloodGroup: UITextField!
    
    @IBOutlet weak var dateOfBirthTF: UITextField!
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    @IBAction func LoginBT(sender: AnyObject) {
        
        self.firstNameTF.resignFirstResponder()
        self.LastNameTF.resignFirstResponder()
        self.PhoneTF.resignFirstResponder()
        self.EmailAddressTF.resignFirstResponder()
        self.dateOfBirthTF.resignFirstResponder()
        //        if emailTF.text!.isEmpty{
        //            displayMessage("Please enter email address ")
        //        }
        //        if passwordTF.text!.isEmpty
        //        {
        //            displayMessage("Please enter password")
        //        }
        
        
    }
    func textFieldShouldReturn(textField:UITextField) -> Bool
    {
        self.firstNameTF.resignFirstResponder()
        self.LastNameTF.resignFirstResponder()
        self.PhoneTF.resignFirstResponder()
        self.EmailAddressTF.resignFirstResponder()
        self.dateOfBirthTF.resignFirstResponder()
        
        return true
    }
    @IBOutlet weak var BloodGroupDropDown: UIPickerView!
    //create list
    var list = ["O-","O+","A-","A+","B-","B+","AB-","AB+"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // dateOfBirthTF.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //you can copy and paste this
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return list.count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        self.view.endEditing(true)
        return list[row]
        
        
        
    }
    
    @IBAction func SaveBT(sender: AnyObject) {
        
        
        if firstNameTF.text!.isEmpty{
            displayMessage("Please enter first name  ")
        }
        if LastNameTF.text!.isEmpty
        {
            displayMessage("Please enter last name ")
        }
        if PhoneTF.text!.isEmpty{
            displayMessage("Please enter phone number ")
        }
        else
        {
            
        }
        if EmailAddressTF.text!.isEmpty
        {
            displayMessage("Please enter email address ")
        }
        if BloodGroup.text!.isEmpty{
            displayMessage("Please enter blood group  ")
        }
        if dateOfBirthTF.text!.isEmpty
        {
            displayMessage("Please enter date of birth ")
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.BloodGroup.text = self.list[row]
        self.BloodGroupDropDown.hidden = true
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.BloodGroup {
            self.BloodGroupDropDown.hidden = false
            //if you dont want the users to se the keyboard type:
            
            // textField.endEditing(true)
        }
        if textField == dateOfBirthTF
        {
            let datePicker = UIDatePicker()
            textField.inputView = datePicker
            datePicker.addTarget(self, action: #selector(SignUpViewController.datePickerChanged(_:)), forControlEvents: .ValueChanged)
        }
        
        
    }
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    func datePickerChanged(sender:UIDatePicker)
    {
        let formatter = NSDateFormatter()
        formatter.dateStyle = .FullStyle
        dateOfBirthTF.text = formatter.stringFromDate(sender.date)
    }
    
    
}
