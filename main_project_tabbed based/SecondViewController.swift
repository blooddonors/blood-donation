//
//  SecondViewController.swift
//  main_project_tabbed based
//
//  Created by Nusum,Koti Reddy on 10/6/16.
//  Copyright © 2016 Nusum,Koti Reddy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    
    
    @IBOutlet weak var emailTF: UITextField!
    
    
    
    
    @IBOutlet weak var passwordTF: UITextField!
    
    func textFieldShouldReturn(textField:UITextField) -> Bool
    {
        self.emailTF.resignFirstResponder()
        self.passwordTF.resignFirstResponder()
        
        
        return true
    }
    

    @IBAction func LoginBT(sender: AnyObject) {
        
        
        
        if emailTF.text!.isEmpty{
        displayMessage("Please enter email address ")
        }
        if passwordTF.text!.isEmpty
        {
          displayMessage("Please enter password")
        }
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

